*********@dnsdb02do:~$ cat /usr/local/scripts/sqlbackup
#!/bin/sh
ThePath=/var/sqlbackup
DATETAG=dnsdb02do-mysql-`date -I`
echo ""
echo "#### begin dnsdb02do mysql-server backup ####"
echo ""
echo "dumping all databases"
touch $ThePath/$DATETAG
mysqldump -u root -ptcotC11 -l --databases domaindb >> $ThePath/$DATETAG
ls -lah $ThePath/$DATETAG
echo "compressing files"
gzip $ThePath/$DATETAG
chown root:root $ThePath/$DATETAG.gz
chmod 755 $ThePath/$DATETAG.gz
find $ThePath -name "*.gz" -mtime +21 -exec rm {} \;
ls -lah $ThePath/
echo ""
echo "#### end dnsdb02do mysql-server backup ####"
echo ""

------------------------------------------------------------------------
********@dnsdb02do:/etc/cron.d$ cat sqlbackup
# m h dom mon dow  user command
35 1 * * * root /usr/local/scripts/sqlbackup

------------------------------------------------------------


this is what we are getting email

#### begin dnsdb02do mysql-server backup ####

dumping all databases
-rw-r--r-- 1 root root 187M Mar 19 01:35 /var/sqlbackup/dnsdb02do-mysql-2023-03-19
compressing files
total 369M
drwxr-xr-x  2 root root 4.0K Mar 19 01:35 .
drwxr-xr-x 14 root root 4.0K Nov  5  2013 ..
-rwxr-xr-x  1 root root  17M Feb 26 01:35 dnsdb02do-mysql-2023-02-26.gz
-rwxr-xr-x  1 root root  17M Feb 27 01:35 dnsdb02do-mysql-2023-02-27.gz
-rwxr-xr-x  1 root root  17M Feb 28 01:35 dnsdb02do-mysql-2023-02-28.gz
-rwxr-xr-x  1 root root  17M Mar  1 01:35 dnsdb02do-mysql-2023-03-01.gz
-rwxr-xr-x  1 root root  17M Mar  2 01:35 dnsdb02do-mysql-2023-03-02.gz
-rwxr-xr-x  1 root root  17M Mar  3 01:35 dnsdb02do-mysql-2023-03-03.gz
-rwxr-xr-x  1 root root  17M Mar  4 01:35 dnsdb02do-mysql-2023-03-04.gz
-rwxr-xr-x  1 root root  17M Mar  5 01:35 dnsdb02do-mysql-2023-03-05.gz
-rwxr-xr-x  1 root root  17M Mar  6 01:35 dnsdb02do-mysql-2023-03-06.gz
-rwxr-xr-x  1 root root  17M Mar  7 01:35 dnsdb02do-mysql-2023-03-07.gz
-rwxr-xr-x  1 root root  17M Mar  8 01:35 dnsdb02do-mysql-2023-03-08.gz
-rwxr-xr-x  1 root root  17M Mar  9 01:35 dnsdb02do-mysql-2023-03-09.gz
-rwxr-xr-x  1 root root  17M Mar 10 01:35 dnsdb02do-mysql-2023-03-10.gz
-rwxr-xr-x  1 root root  17M Mar 11 01:35 dnsdb02do-mysql-2023-03-11.gz
-rwxr-xr-x  1 root root  17M Mar 12 01:35 dnsdb02do-mysql-2023-03-12.gz
-rwxr-xr-x  1 root root  17M Mar 13 01:35 dnsdb02do-mysql-2023-03-13.gz
-rwxr-xr-x  1 root root  17M Mar 14 01:35 dnsdb02do-mysql-2023-03-14.gz
-rwxr-xr-x  1 root root  17M Mar 15 01:35 dnsdb02do-mysql-2023-03-15.gz
-rwxr-xr-x  1 root root  17M Mar 16 01:35 dnsdb02do-mysql-2023-03-16.gz
-rwxr-xr-x  1 root root  17M Mar 17 01:35 dnsdb02do-mysql-2023-03-17.gz
-rwxr-xr-x  1 root root  17M Mar 18 01:35 dnsdb02do-mysql-2023-03-18.gz
-rwxr-xr-x  1 root root  17M Mar 19 01:35 dnsdb02do-mysql-2023-03-19.gz

#### end dnsdb02do mysql-server backup ####

------------------------------------------------------------------------------